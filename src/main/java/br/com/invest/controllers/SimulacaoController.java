package br.com.invest.controllers;

import br.com.invest.models.Simulacao;
import br.com.invest.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @GetMapping
    public Iterable<Simulacao> retornarTodasSimulacoes() {
        try {
            Iterable<Simulacao> simulacoes = simulacaoService.retornarTodasSimulacoes();
            return simulacoes;
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
