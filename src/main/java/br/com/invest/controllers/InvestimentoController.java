package br.com.invest.controllers;

import br.com.invest.models.Investimento;
import br.com.invest.models.Simulacao;
import br.com.invest.models.dtos.RendimentoDTO;
import br.com.invest.services.InvestimentoService;
import br.com.invest.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento inserirInvestimento(@RequestBody Investimento investimento) {
        try {
            return investimentoService.inserirInvestimento(investimento);
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PostMapping("/{id}/simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    public RendimentoDTO executarSimulacao(@PathVariable(name = "id") int id, @RequestBody Simulacao simulacao) {
        try {
            Investimento investimento = investimentoService.retornarInvestimentoPorId(id);
            RendimentoDTO rendimentoDTO = simulacaoService.executarSimulacao(simulacao, investimento);
            return rendimentoDTO;
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping
    public Iterable<Investimento> retornarTodosInvestimentos() {
        try {
            return investimentoService.retornarTodosInvestimentos();
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }

    }
}
