package br.com.invest.services;

import br.com.invest.models.Investimento;
import br.com.invest.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    InvestimentoRepository investimentoRepository;

    public Investimento inserirInvestimento(Investimento investimento) {
        Investimento investimentoObjeto = investimentoRepository.save(investimento);
        return investimento;
    }

    public Iterable<Investimento> retornarTodosInvestimentos() {
        Iterable<Investimento> todosInvestimentos = investimentoRepository.findAll();
        return todosInvestimentos;
    }

    public Investimento retornarInvestimentoPorId(int id) {
        Optional<Investimento> investimento = investimentoRepository.findById(id);
        if (investimento.isPresent()) {
            return investimento.get();
        }
        throw new RuntimeException("Investimento não encontrado");
    }
}
