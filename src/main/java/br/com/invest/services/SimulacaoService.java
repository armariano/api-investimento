package br.com.invest.services;

import br.com.invest.models.Investimento;
import br.com.invest.models.Simulacao;
import br.com.invest.models.dtos.RendimentoDTO;
import br.com.invest.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;



    public Iterable<Simulacao> retornarTodasSimulacoes(){
        Iterable<Simulacao> simulacoes = simulacaoRepository.findAll();
        return simulacoes;
    }

    public RendimentoDTO executarSimulacao(Simulacao simulacao, Investimento investimento){

        //salvar simulacao
        simulacao.setInvestimento(investimento);
        simulacaoRepository.save(simulacao);

        RendimentoDTO rendimentoDTO = new RendimentoDTO();
        double juros;
        DecimalFormat formato = new DecimalFormat("#.##");
        List<Double> valores = new ArrayList<>();

        rendimentoDTO.setRendimentoPorMes(investimento.getRendimentoAoMes());

        for (int i =1; i <= simulacao.getQuantidadeMeses(); i++){
            if(i == 1){
                juros = (simulacao.getValorAplicado() * investimento.getRendimentoAoMes()) / 100;
                rendimentoDTO.setMontante(juros + simulacao.getValorAplicado());
            }else{
                juros = (rendimentoDTO.getMontante() * investimento.getRendimentoAoMes()) / 100;
                rendimentoDTO.setMontante(juros + rendimentoDTO.getMontante());
            }
        }
        return rendimentoDTO;
    }
}
