package br.com.invest.models;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.text.DecimalFormat;
import java.util.List;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Nome do interessado não pode ser nulo")
    @NotBlank(message = "Nome do interessado  não pode ser vázio")
    private String nomeDoInteressado;

    @NotNull(message = "Email não pode ser nulo")
    @NotBlank(message = "Email não pode ser vázio")
    @Email(message = "Email com formato inválido")
    private String email;

    @NotNull(message = "Valor aplicado não pode ser nulo")
    @Digits(integer = 6, fraction = 2, message = "Valor com formato inválido")
    private Double valorAplicado;

    @NotNull(message = "Valor aplicado não pode ser nulo")
    private int quantidadeMeses;

    @ManyToOne(cascade = CascadeType.ALL)
    Investimento investimento;

    public Simulacao() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeDoInteressado() {
        return nomeDoInteressado;
    }

    public void setNomeDoInteressado(String nomeDoInteressado) {
        this.nomeDoInteressado = nomeDoInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(Double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }
}
