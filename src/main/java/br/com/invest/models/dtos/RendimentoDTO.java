package br.com.invest.models.dtos;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class RendimentoDTO {
    private Double rendimentoPorMes;

    private Double montante;

    public RendimentoDTO() {
    }

    public Double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(Double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public Double getMontante() {
        return montante;
    }

    public void setMontante(Double montante) {
        BigDecimal bd = new BigDecimal(montante).setScale(2, RoundingMode.FLOOR);
        this.montante = bd.doubleValue();
    }
}
