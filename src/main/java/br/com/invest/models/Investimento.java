package br.com.invest.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    @NotNull(message = "Nome do investimento não pode ser nulo")
    @NotBlank(message = "Nome do investimento não pode ser vázio")
    private String nome;

    @NotNull(message = "Rendimento ao mês não pode ser nulo")
    private Double rendimentoAoMes;

    public Investimento() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(Double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }
}
